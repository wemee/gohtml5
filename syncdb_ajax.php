<?php
function cmpindex($a, $b) {
	global $idx;
	if($a[$idx]==$b[$idx]) {
		return 0;
	} else {
		return ($a[$idx]>$b[$idx]) ? 1 : -1;
	}
}

function filter($data, $idx, $opr, $val) {
	global $data, $idx, $opr, $val;
	$filtered = array();

	foreach($data as $datum) {
		switch($opr) {
			case '=':  $match = $datum[$idx]==$val; break;
			case '<':  $match = $datum[$idx]<$val;  break;
			case '>':  $match = $datum[$idx]>$val;  break;
			case '<=': $match = $datum[$idx]<=$val; break;
			case '>=': $match = $datum[$idx]>=$val; break;
		}
		if($match) $filtered[] = $datum;
	}

	usort($filtered,'cmpindex');
	return $filtered;
}

function isModified() {
	global $faketime;

	if(isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])) {
		$stamp_svr = strtotime($faketime);
		$stamp_cli = strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']);
		if($stamp_svr <= $stamp_cli) return false;
	}

	return true;
}

$faketime = 'Thu, 11 Jul 2013 00:00:00 GMT';
$idx = $_POST['idx'];
$opr = $_POST['opr'];
$val = $_POST['val'];

$i = 1;
$data = array(
	array('id' => $i++, 'name' => "John", 'age' => 41, 'score' => 10.1, 'enabled' => true),
	array('id' => $i++, 'name' => "Peter", 'age' => 24, 'score' => 20.5, 'enabled' => true),
	array('id' => $i++, 'name' => "Mary", 'age' => 13, 'score' => 1.71, 'enabled' => false),
	array('id' => $i++, 'name' => "Viona", 'age' => 29, 'score' => 30.99, 'enabled' => false),
	array('id' => $i++, 'name' => "Grace", 'age' => 31, 'score' => 99.99, 'enabled' => true),
	array('id' => $i++, 'name' => "Raymond", 'age' => 32, 'score' => 4.0, 'enabled' => true),
	array('id' => $i++, 'name' => "[BAD-0]", 'age' => 0, 'score' => 0, 'enabled' => true),
	array('id' => $i++, 'name' => "[BAD-1]", 'age' => true, 'score' => false, 'enabled' => 1)
);

ob_start();
sleep(3);

header('Cache-Control: max-age=15; must-revalidate');

if(isModified()) {
	$httpcode = 200;
	header('Content-Type: application/json', true);
	echo json_encode(filter($data,$idx,$opr,$val));
} else {
	$httpcode = 304;
}

header("Last-Modified: $faketime", true, $httpcode);

ob_end_flush();
?>