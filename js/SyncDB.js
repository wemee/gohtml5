/**
 * 同步化資料庫
 *
 * @class    SyncDB
 * @author   Raymond Wu
 */
var SyncDB = function(dbname, tables, version) {
	"use strict";

	//var Logger;
	var IDB_PK = "id";

	// IndexDB 快取時效
	var IDB_TIMEOUT  = (location.hostname==="localhost") ? 5 : 30;

	// IDB 狀態值&參數值
	var idb_tested   = false;
	var idb_linked   = false;
	var idb_database = null;
	var syncdb       = this;

	// 屬性
	this.ajaxURL = "";

	// 事件
	this.read_ok    = function() {};
	this.read_error = function() {};
	this.save_ok    = function() {};
	this.save_error = function() {};
	this.sync_begin = function() {}; // 已實作
	this.sync_end   = function() {}; // 已實作

	/**
	 * 儲存物件
	 *
	 * @param {string} stname ObjectStore 名稱
	 * @param {object} obj 物件資料
	 */
	this.save = function(stname, obj) {
		if(idb_tested) {
			if(idb_linked) {
				obj.mtime = new Date().getTime();
				var trans = idb_database.transaction(stname, "readwrite");
				var store = trans.objectStore(stname);
				try {
					var req = store.put(obj);
					req.onsuccess = function() {
						var msg = "IndexedDB 儲存成功 (obj.id={id})";
						msg = msg.replace("{id}", obj.id);
						Logger.info(msg);
					};
					req.onerror = function() {
						var msg = "IndexedDB 儲存失敗 (obj.id={id})";
						msg = msg.replace("{id}", obj.id);
						Logger.info(msg);
					};
				} catch(e) {
					Logger.debug("Transaction 建立失敗: "+e.message);
				}
			} else {
				Logger.info("IndexedDB 無法使用，不處理儲存動作");
			}
		} else {
			// 如果 IndexedDB 還沒檢查完，延後處理
			if(window.Logger) Logger.info("IndexedDB 尚未連線完成，延遲 0.333 秒再儲存");
			setTimeout(function() { syncdb.save(stname,obj); }, 333);
		}
	};

	/**
	 * 載入物件
	 *
	 * @param {string} stname ObjectStore 名稱
	 * @param {string} cond 條件式
	 * @param {function} callback 資料接收函數
	 */
	this.load = function(stname, cond, callback) {
		if(idb_tested) {
			if(idb_linked) {
				try {
					var trans = idb_database.transaction(stname,"readonly");
					var store = trans.objectStore(stname);
					var req   = parseToCursor(store, cond);

					if(req!==null) {
						var i = 0;
						var objects = [];
						var now = new Date().getTime();

						req.onsuccess = function(event) {
							var cursor = event.target.result;
							if (cursor) {
								// cursor 讀取中
								var obj = cursor.value;

								// 檢查 mtime，決定是否啟動 Ajax 功能
								if(syncdb.ajaxURL!=="") {
									var secdiff = Math.floor((now-obj.mtime)/1000);
									if(secdiff > IDB_TIMEOUT) {
										// debug
										var mtime_2822 = new Date(obj.mtime).toGMTString();
										var now_2822   = new Date(now).toGMTString();
										Logger.info("IndexedDB 資料逾時, 秒差: "+secdiff);
										Logger.debug("  now="+now+" / "+now_2822);
										Logger.debug("mtime="+obj["mtime"]+" / "+mtime_2822);

										objects = null;
										loadByAjax(stname,cond,callback,mtime_2822);
									}
								}

								// 沒啟用 Ajax 功能就正常讀取
								if(objects!==null) {
									var msg = "IndexedDB 讀取成功 (obj.id={id})";
									msg = msg.replace("{id}", obj.id);
									Logger.info(msg);
									objects.push(cursor.value);
									cursor.continue();
								}
							} else {
								// cursor 讀取完畢
								if(objects.length) {
									Logger.info("IndexedDB 讀取結束");
									callback(objects);
								} else {
									Logger.info("IndexedDB 查無資料");
									loadByAjax(stname,cond,callback);
								}
							}
						};

						req.onerror = function() {
							Logger.error("IndexedDB 查詢錯誤");
							callback([]);
						};
					} else {
						Logger.error("IndexedDB 查詢錯誤");
						callback([]);
					} // if(req!=null)
				} catch(e) {
					Logger.error("不預期的錯誤: " + e.message);
				}
			} else {
				Logger.error("沒有 IndexedDB 連線");
			}
		} else {
			if(window.Logger) Logger.info("IndexedDB 尚未連線完成，延遲 0.333 秒再載入");
			setTimeout(function() {
				syncdb.load(stname, cond, callback);
			}, 333);
		}
	};

	/**
	 * 顯示資料在文件尾端
	 *
	 * @param {string} stname ObjectStore 名稱
	 * @param {string} cond 條件式
	 */
	this.dump = function(stname, cond) {
		this.load(stname, cond, function(objs) {
			var i, k;
			var fields = [];

			for(k in objs[0]) {
				if(k!=="mtime") fields.push(k);
			}

			var htmlcode = ""
			htmlcode += "<p>" + stname +": (" + cond + ")</p>";
			htmlcode += '<p><table>';
			htmlcode += "<tr>";
			for(i in fields) {
				htmlcode += "<td>" + fields[i] + "</td>";
			}
			htmlcode += "</tr>";

			for(i in objs) {
				var obj = objs[i];
				htmlcode += "<tr>";
				for(k in obj) {
					if(k!="mtime") {
						htmlcode += "<td>" + obj[k] + "</td>";
					}
				}
				htmlcode += "</tr>";
			}

			htmlcode += "</table></p>";
			document.body.innerHTML += htmlcode;
		});
	};

	/**
	 * 將條件式分析後轉換成 IDBKeyRange，供 IndexedDB 處理
	 * https://developer.mozilla.org/en-US/docs/Web/API/IDBKeyRange?redirectlocale=en-US&redirectslug=IndexedDB%2FIDBKeyRange
	 *
	 * @param store ObjectStore 物件
	 * @param cond  條件式
	 */
	var parseToCursor = function(store, cond) {
		var pattern = /^(\w+)\s*(=|<|>|>=|<=)\s*([^=].*)$/;
		var matches = pattern.exec(cond);

		if(matches!==null) {
			var idx = matches[1];
			var opr = matches[2];
			var val = matches[3];
			var range = null;

			if(/^".*"$/.test(val)) {
				val = val.substring(1,val.length-1);
			} else if(/^'.*'$/.test(val)) {
				val = val.substring(1,val.length-1);
			} else if(/^[0-9]+$/.test(val)) {
				val = parseInt(val,10);
			} else if(/^[0-9]+\.[0-9]+$/.test(val)) {
				val = parseFloat(val);
			} else if(/^true|false$/.test(val)) {
				Logger.error("boolean 值無法作為索引欄位");
			} else {
				Logger.error("不預期的資料型態: idx="+idx+"/opr="+opr+"/val="+val);
			}

			switch(opr) {
				case "=":  range = IDBKeyRange.only(val); break;
				case ">":  range = IDBKeyRange.lowerBound(val,true); break;
				case "<":  range = IDBKeyRange.upperBound(val,true); break;
				case ">=": range = IDBKeyRange.lowerBound(val); break;
				case "<=": range = IDBKeyRange.upperBound(val); break;
			}

			if(range!=null) {
				var req;
				if(idx==IDB_PK) {
					req = store.openCursor(range);
				} else {
					var index = store.index(idx);
					req = index.openCursor(range);
				}
				return req;
			} else {
				Logger.error("KeyRange 無法產生");
			}
		} else {
			Logger.error("條件式語法錯誤: " + cond);
		}

		return null;
	};

	/**
	 * 使用 Ajax 方式抓資料
	 *
	 * @param stname   ObjectStore 名稱
	 * @param cond     條件式
	 * @param callback 資料接收函數
	 *
	 * @todo IE8 相容設計
	 * @todo request 失敗處理 (#31)
	 * @todo cond parse 抽離 (#32)
	 */
	var loadByAjax = function(stname, cond, callback, since) {
		if(syncdb.ajaxURL==="") {
			return;
		}

		// 呼叫同步化開始事件
		if(typeof(syncdb.sync_begin)==="function") {
			syncdb.sync_begin();
		}

		var pattern = /^(\w+)\s*(=|<|>|>=|<=)\s*([^=].*)$/;
		var matches = pattern.exec(cond);

		if (matches!==null) {
			var idx = matches[1];
			var opr = matches[2];
			var val = matches[3];

			if(/^".*"$/.test(val)) {
				val = val.substring(1,val.length-1);
			} else if(/^'.*'$/.test(val)) {
				val = val.substring(1,val.length-1);
			} else if(/^[0-9]+$/.test(val)) {
				val = parseInt(val,10);
			} else if(/^[0-9]+\.[0-9]+$/.test(val)) {
				val = parseFloat(val);
			} else if(/^true|false$/.test(val)) {
				Logger.error("boolean 值無法作為索引欄位");
			} else {
				Logger.error("不預期的資料型態: idx="+idx+"/opr="+opr+"/val="+val);
			}

			// Ajax 回應流程
			var xhr = new XMLHttpRequest();
			xhr.onreadystatechange = function() {
				if(xhr.readyState==4) {
					// 呼叫同步化結束事件
					if(typeof(syncdb.sync_end)=="function") {
						syncdb.sync_end();
					}

					switch(xhr.status) {
						case 200:
							try {
								var objs = eval(xhr.responseText);
								Logger.debug("Ajax 回傳值分析完成");
							} catch(e) {
								Logger.error("Ajax 回傳不是 JSON 格式" + xhr.responseText);
							}

							// 更新資料延遲存入 IDB (不阻斷設計)
							if(idb_linked) {
								setTimeout(function() {
									Logger.debug("物件寫回 IndexedDB");
									for(var i in objs) syncdb.save(stname, objs[i]);
								}, 1000);
							}

							// 更新資料回傳至客戶端
							callback(objs);
							break;
						case 304:
							// 先更新相關資料的 mtime, 然後回傳這些資料
							//log(INFO, "資料沒異動 (HTTP/1.1 304 Not Modified)");
							touch(stname, cond);
							setTimeout(function() { syncdb.load(stname, cond, callback); }, 100);
							break;
						default:
							//log(ERROR, "xhr.status="+xhr.status);
					}
				} else {
					//log(DEBUG, "xhr.readyState="+xhr.readyState);
				}
			};

			// 整理 POST 資料
			var content = "";
			content += "idx="  + escape(idx);
			content += "&opr=" + escape(opr);
			content += "&val=" + escape(val);

			// 發送 POST 封包
			xhr.open("POST", syncdb.ajaxURL, true);
			if(since!=undefined) {
				xhr.setRequestHeader("If-Modified-Since", since);
			}
			xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xhr.send(content);
		}
	};

	/**
	 * Browser 支援 IndexedDB 但是還沒建索引
	 *
	 * @param event IndexedDB 版本異動事件
	 */
	var upgradeTable = function(event) {
		var idb = event.currentTarget.result;

		// 如何不透過 createObjectStore 取得 store
		for(var table in tables) {
			var ost = idb.createObjectStore(table, { keyPath: IDB_PK });
			var indices = tables[table];
			for(var idx in indices) {
				var idx_type = indices[idx];
				ost.createIndex(idx, idx, {unique: (idx_type=="u")});
			}

			// mtime
			ost.createIndex("mtime", "mtime");
		}
	};

	/**
	 * 連線到資料庫
	 */
	var connect = function() {
		if(window.indexedDB!=undefined) {
			// 取得 IDB 連線建立工廠
			var IDBFactory = window.indexedDB;

			// 東方不敗，砍掉重練
			if(version==-1) {
				IDBFactory.deleteDatabase(dbname);
				version = 1;
				Logger.debug("移除資料庫 "+dbname);
			}

			// 使用兩參數的方式 open
			if(version==undefined) {
				version = 1;
				Logger.debug("未定義版本，預設為 1");
			}

			var conn_req = IDBFactory.open(dbname,version);
			conn_req.onerror = function() {
				idb_tested = true;
				Logger.error("資料庫 "+dbname+" 連線失敗");
			};
			conn_req.onsuccess = function() {
				idb_database = this.result;
				idb_tested = true;
				idb_linked = true;
				Logger.info("資料庫 "+dbname+" 連線成功");
			};
			conn_req.onupgradeneeded = upgradeTable;
		} else {
			idb_tested = true;
			Logger.warning("瀏覽器不支援 IndexedDB");
		}
	};

	/**
	 * 摸物件, 僅供 loadByAjax 使用, 不做連線檢查以及其他檢查
	 *
	 * @param stname ObjectStore 名稱
	 * @param cond   條件式
	 */
	var touch = function(stname, cond) {
		var trans = idb_database.transaction(stname,"readwrite");
		var store = trans.objectStore(stname);
		var req   = parseToCursor(store, cond);
		if(req!=null) {
			req.onsuccess = function(event) {
				var cursor = event.target.result;
				if (cursor) {
					var obj = cursor.value;
					syncdb.save(stname, obj);
					cursor.continue();
				}
			};
		}
	};

	/**
	 * 載入相依套件
	 */
	var include = function(idx) {
		var modules = [
			"Logger",
			"JSTester"
		];

		if(idx==undefined) idx = 0;
		if(idx<modules.length) {
			var tags = document.getElementsByTagName("head");
			if(tags.length>0) {
				var head   = tags[0];
				var cnode  = document.createElement("script");
				var module = modules[idx];
				cnode.type = "text/javascript";
				cnode.src  = "js/{class}.js".replace("{class}",modules[idx]);
				head.appendChild(cnode);

				var itvcheck = setInterval(function() {
					if(window[module]!=undefined) {
						console.log('"{m}" loaded'.replace("{m}",module));
						clearInterval(itvcheck);
						if(idx+1<modules.length) {
							include(idx+1);
						} else {
							connect();
						}
					} else {
						// TODO: 還在載入 (有可能根本不存在 XD)
					}
				}, 100);
			} else {
				// TODO: 沒有 <head>
			}
		} else {
			// TODO: modules===[]
		}
	};

	include();
};