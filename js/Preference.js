/**
 * 管理客戶端設定值的工具物件
 *
 * Known Issue
 * - Chrome 在單機上無法寫 cookie 但是可以寫 storage
 * - Firefox 在單機上可以寫 cookie / storage
 * - IE10 模擬 IE7 會有 localStorage 但是不能用
 * - IE Cookie 分解還有 Bug
 *
 * session 保存到瀏覽器關閉 (如果不支援 HTML5 storage 則強制使用 cookie)
 * local   永久保存 (如果不支援 HTML5 storage 則強制使用 cookie)
 * cookie  可供 server 存取，但是比較消耗流量，儲存時採用 URL Encode
 */
try {

var Preference = new function() {

	/* 儲存方式設定 */
	var DEFAULT_SCOPE = "session";
	var OLD_SCOPE = "cookie";
	var ALL_SCOPE = ["session","local","cookie"];

	/* COOKIE 有效範圍/生命週期 */
	var COOKIE_EXPDAY = 7;
	var COOKIE_DOMAIN = location.hostname;
	var COOKIE_PATH   = "/";

	/* 檢查是否支援 W3C Web Storage */
	var HAS_STORAGE;
	var idx = navigator.userAgent.indexOf("MSIE");
	if(idx==-1) {
		// 非 IE 使用直接檢查法
		HAS_STORAGE = (typeof(window.sessionStorage)=="object");
	} else {
		// IE 使用版本檢查法
		// 因為在 <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" /> 的情境會誤判
		// 或是點 "相容性檢視" 也會踩到
		var bidx = idx+5;
		var eidx = navigator.userAgent.indexOf(";",bidx);
		var iever = parseFloat(navigator.userAgent.substring(bidx,eidx));
		HAS_STORAGE = (iever>=8);
	}

	/* 每個設定值的生命週期定義 */
	this.scopeList = {};

    /* 每個設定值的Label */
	this.labelList = {};

	/**
	 * 讀取 Cookie
	 */
	var getByCookie = function(key) {
		key += "=";
		var cookie = document.cookie;
		var bpos = cookie.indexOf(key);

		if(bpos!=-1) {
			bpos += key.length;
			var epos = cookie.indexOf(";",bpos);
			if(epos==-1) epos = cookie.length;
			var val = unescape(cookie.substring(bpos,epos));
			return val;
		}

		return "";
	};

	/**
	 * 設定 Cookie
	 */
	var setByCookie = function(key, val) {
		var domain  = COOKIE_DOMAIN;
		var expires = new Date();

		if(domain=="localhost") domain="";

		expires.setTime(
			expires.getTime() +
			(86400*1000*COOKIE_EXPDAY)
		);

		var stmt = "{key}={value}; domain={domain}; path={path}; expires={expires}";
		stmt = stmt.replace("{key}", key);
		stmt = stmt.replace("{value}", escape(val));
		stmt = stmt.replace("{domain}", domain);
		stmt = stmt.replace("{path}", COOKIE_PATH);
		stmt = stmt.replace("{expires}", expires.toUTCString());
		document.cookie = stmt;
	};

	/**
	 * 移除 Cookie
	 */
	var dropByCookie = function(key) {
		var domain  = COOKIE_DOMAIN;
		var expires = new Date(0);

		if(domain=="localhost") domain="";

		var stmt = "{key}=; domain={domain}; path={path}; expires={expires}";
		stmt = stmt.replace("{key}", key);
		stmt = stmt.replace("{domain}", domain);
		stmt = stmt.replace("{path}", COOKIE_PATH);
		stmt = stmt.replace("{expires}", expires.toUTCString());
		document.cookie = stmt;
	};

	/**
	 * 取得某項設定值的生命週期
	 */
	this.getScope = function(key) {
		var scope = this.scopeList[key];
		if(scope==undefined) {
			scope = HAS_STORAGE ? DEFAULT_SCOPE : OLD_SCOPE;
		}
		return scope;
	};

	/**
	 * 讀取設定值
	 */
	this.get = function(key) {
		var scope = this.getScope(key);
		if(scope=="cookie") {
			getByCookie(key);
		} else {
			var st = (scope=="session") ? window.sessionStorage : window.localStorage;
			return st[key];
		}
	};

	/**
	 * 寫入設定值
	 */
	this.set = function(key, val) {
		var scope = this.getScope(key);
		if(scope=="cookie") {
			setByCookie(key,val);
		} else {
			var st = (scope=="session") ? window.sessionStorage : window.localStorage;
			st[key] = val;
		}
	};

	/**
	 * 移除設定值
	 */
	this.drop = function(key) {
		var scope = this.getScope(key);
		if(scope=="cookie") {
			dropByCookie(key);
		} else {
			var st = (scope=="session") ? window.sessionStorage : window.localStorage;
			delete st[key];
		}
	};

	/**
	 * 設定值全部移除
	 */
	this.dropAll = function(scope) {
		if(scope==undefined) scope = "all";

		if(HAS_STORAGE) {
			if(scope=="session" || scope=="all") {
				for(key in window.sessionStorage) {
					delete window.sessionStorage[key];
				}
			}

			if(scope=="local" || scope=="all") {
				for(key in window.localStorage) {
					delete window.localStorage[key];
				}
			}
		}

		if(scope=="cookie" || scope=="all") {
			if(document.cookie!="") {
				var eqp;
				var pairs = document.cookie.split("; ");
				for(i in pairs) {
					kvp = pairs[i];
					eqp = kvp.indexOf("=");
					key = kvp.substring(0,eqp);
					dropByCookie(key);
				}
			}
		}
	};

	/**
	 * 恢復原廠設定
	 */
	this.restore = function(factory) {
		this.dropAll();

		var key, val;
		if(typeof(factory)=="object") {
			for(key in factory) {
				val = factory[key];
				this.set(key,val);
			}
		}
	};

	/**
	 * 產生設定值報告
	 */
	this.dump = function() {
		var html_out, key, val;

		// dump session
		html_out = "<h3>window.sessionStorage</h3>";
		if(HAS_STORAGE) {
			html_out += '<p><table border="1">';
			html_out += "<tr><th>鍵</th><th>值</th></tr>";
			for(key in window.sessionStorage) {
				val = window.sessionStorage[key];
				html_out += "<tr><td>" + key + "</td><td>" + val + "</td></tr>" ;
			}
			html_out += "</table></p><hr/>";

		} else {
			html_out += "<p>不支援 window.sessionStorage</p>";
		}
		document.write(html_out);

		// dump local
		html_out = "<h3>window.localStorage</h3>";
		if(HAS_STORAGE) {
			html_out += '<p><table border="1">';
			html_out += "<tr><th>鍵</th><th>值</th></tr>";
			for(key in window.localStorage) {
				val = window.localStorage[key];
				html_out += "<tr><td>"+key+"</td><td>"+val+"</td></tr>";
			}
			html_out += "</table></p><hr/>";
		} else {
			html_out += "<p>不支援 window.localStorage</p>";
		}
		document.write(html_out);

		// dump cookie
		html_out = "<h3>document.cookie</h3>";
		html_out += '<p>localhost 環境可能無法使用 document.cookie</p>';
		html_out += '<p><table border="1">';
		html_out += "<tr><th>鍵</th><th>值</th></tr>";

		if(document.cookie!="") {
			var eqp;
			var pairs = document.cookie.split("; ");
			for(i in pairs) {
				kvp = pairs[i];
				eqp = kvp.indexOf("=");
				key = kvp.substring(0,eqp);
				val = unescape(kvp.substring(eqp+1));
				html_out += "<tr><td>"+key+"</td><td>"+val+"</td></tr>";
			}
		}

		html_out += "</table></p><hr/>";
		document.write(html_out);
	};


   /**
    *定義一項偏好設定
    */
	this.define = function(key, scope, label){
	    this.define_entry(key, scope, lable);
	};

   /**
    *定義多項偏好設定
    */
	this.define = function (defs) {
	    for (key in defs) {
	        for (val in defs[key]) {
                if(val == 0 || val =="scope")
                    var scope = defs[key][val];

                if(val == 1 || val =="label")
                    var label = defs[key][val];
	        }

	        this.define_entry(key, scope,label);
	    }
	};

   /**
    *內部使用，給定偏好設定值
    */
	this.define_entry = function(key,scope,label){
	    // 注意!! 這一段不可以使用 Array.indexOf，否則會造成 IE8- 錯誤
	    var good_scope = false;
	    for(i in ALL_SCOPE) {
	        if(ALL_SCOPE[i]==scope) {
	            good_scope = true;
	            break;
	        }
	    }

	    if(good_scope) {
	        // 如果 HTML5 storage 不存在，強制使用 cookie
	        if(scope!="cookie" && !HAS_STORAGE) {
	            scope = "cookie";
	        }
	        this.scopeList[key] = scope;
	        this.labelList[key] = label;
	    } else {
	        console.warn("scope 必須是 " + ALL_SCOPE.join(",") + " 等字串，忽略 " + key + " 的 scope 設定");
	    }
	};
};

} catch(e) { alert("init: "+e.message); }