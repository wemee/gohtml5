// 歷程記錄等級
var ERROR   = 1;
var WARNING = 2;
var INFO    = 3;
var DEBUG   = 4;
var LOG_LEVEL = DEBUG;

/**
 * Console 歷程記錄
 *
 * @param level 重要性 (1:ERROR, 2:WARNING, 3:INFO, 4:DEBUG)
 * @param msg   記錄訊息
 */
var log = function(level, msg) {
    if(level>LOG_LEVEL) return;

    // 產生本地時間字串
    var now = new Date();
    var hh = now.getHours();
    var mm = now.getMinutes();
    var ss = now.getSeconds();
    hh = (hh<10) ? ("0"+hh) : (""+hh);
    mm = (mm<10) ? ("0"+mm) : (""+mm);
    ss = (ss<10) ? ("0"+ss) : (""+ss);
    var time = [hh,mm,ss].join(":");

    // 產生輸出形式
    var out = "[{time}] {msg}";
    out = out.replace("{time}",time);
    out = out.replace("{msg}",msg);

    // 產生錯誤訊息
    switch(level) {
        case ERROR:   console.error(out); break;
        case WARNING: console.warn(out);  break;
        case INFO:    console.log(out);   break;
        case DEBUG:
            if(console.debug==undefined) {
                console.log(out); // for MSIE 10
            } else {
                console.debug(out);
            }
            break;
    };
};

function Point(x, y){
	this.x = x||0;
	this.y = y||0;
}

var polarCoordinate = {
	unipoints : {
		// null, null, null,
		3:[new Point(0,1), new Point(-0.866,-0.5), new Point(0.866,-0.5)],
		4:[new Point(0,1), new Point(-1,0), new Point(0,-1), new Point(1,0)],
		5:[new Point(0,1), new Point(-0.951,0.309), new Point(-0.587,-0.809), new Point(0.587,-0.809), new Point(0.951,0.309)],
		6:[new Point(0,1), new Point(-0.866,0.5), new Point(-0.866,-0.5), new Point(0,-1), new Point(0.866,-0.5), new Point(0.866,0.5)]
	},

	/**
	 * @param radius 半徑(Aarray)
	 * 說明: 極座標角度固定 但半徑不固定時使用
	 * To be implement: 將暫存的數值儲存到 SyncDB
	 */
	getPolygonPoints : function(radius){
		var c = radius.length;
		if(c<3) return;

		if(c in this.unipoints) { // 3~6邊或計算過有暫存
			var points = [];
			var ups = this.unipoints[c];
			for (var i=0; i<c; i++) {
				points.push(new Point(radius[i]*ups[i].x, radius[i]*ups[i].y));
				// this.unipoints[c].forEach(function(p){
				// 	points.push(new Point(r*p.x, r*p.y));
				// })
			};
			return points;
		} else { // 暫存 網頁沒falsh 就不用重新計算
			this.setUnipoint(c);
			return this.getRegularPolygonPoints(c, r);
		}
	},

	/**
	 * @param c 邊數
	 * @param r 半徑
	 *
	 * To be implement:將暫存的數值儲存到 SyncDB
	 */
	getRegularPolygonPoints : function(c, r){
		if(c<3) return;

		if(c in this.unipoints) { // 3~6邊或計算過有暫存
			var points = [];
			this.unipoints[c].forEach(function(p){
				points.push(new Point(r*p.x, r*p.y));
			})
			return points;
		} else { // 暫存 網頁沒falsh 就不用重新計算
			this.setUnipoint(c);
			return this.getRegularPolygonPoints(c, r);
		}
	},

	/**
	 * @param c 邊數
	 *
	 * To be implement:將暫存的數值儲存到 SyncDB
	 *				  :改為Private function
	 */
	setUnipoint : function(c){
		var angle = Math.PI/2;
		var step  = Math.PI*2/c;
		this.unipoints[c] = [];
		for(i=0;i<c;i++) {
			this.unipoints[c].push(
				new Point(Math.cos(angle),Math.sin(angle))
			);
			angle += step;
		}
	}
}

var Painter = {
	fillLG : function(ctx, r, c1, c2, d) {
		var rPoints = polarCoordinate.getRegularPolygonPoints(4, r);
		// 預設值
		if(c1==undefined) c1 = "black";//"#ffffff";
		if(c2==undefined) c2 = "white";//"#000000";
		if(d==undefined)  d  = 2;

		var i1,i2;	
		switch(d) {
			case 0:  i1 = 0; i2 = 3;        // 左 > 右
			case 1:  i1 = 0; i2 = 1; break; // 上 > 下
			case 3:  i1 = 3; i2 = 1; break; // 右上 > 左下
			case 2:                         // 左上 > 右下
			default: i1 = 0; i2 = 2;        // 預設值 2
		}
		var g1Point = rPoints[i1];
		var g2Point = rPoints[i2];
		var grad = ctx.createLinearGradient(g1Point.x,g1Point.y,g2Point.x,g2Point.y);
		grad.addColorStop(0,c1);
		grad.addColorStop(1,c2);
		ctx.fillStyle = grad;
		ctx.fill();
	},

	drawLine : function(ctx, startPoint , endPoint) {
		ctx.beginPath();
		ctx.moveTo(startPoint.x, startPoint.y);
		ctx.lineTo(endPoint.x, endPoint.y);
		ctx.stroke();
	},

	drawLines : function(ctx, points) {
		var startPoint = points[0];
		ctx.beginPath();
		ctx.moveTo(startPoint.x,startPoint.y);
		points.slice(1).forEach(function(p){
			ctx.lineTo(p.x, p.y);
		});
		ctx.stroke();
	},

	drawRadioLines : function(ctx, c,r,oriPoint) {
		if (oriPoint == undefined) oriPoint=new Point(0,0);
		points = polarCoordinate.getRegularPolygonPoints(c,r);
		for(var x in points){
			this.drawLine(ctx, oriPoint, points[x]);
		}
	},

	drawPolygon : function(ctx, points) {
		if(points<3) return;
		points.push(points[0]);
		this.drawLines(ctx, points);
		points.pop();
	},

	drawRegularPolygon : function(ctx, c, r) {
		if(c<3) return;
		var points = polarCoordinate.getRegularPolygonPoints(c,r);
		this.drawPolygon(ctx, points);
	},

	drawText : function(ctx, text, point) {
		ctx.transform(1,0,0,-1,0,0);
		ctx.fillText(text,point.x,-point.y);
		ctx.transform(1,0,0,-1,0,0);
	},

	drawTextArgs : function(ctx, text, args) {
		if (args.textAlign != undefined) {
			var globalAlphaBak = ctx.globalAlpha;
			ctx.globalAlpha = 0;
			this.drawLine(ctx, args.startPoint, args.endPoint);
			ctx.globalAlpha = globalAlphaBak;
			var textAlignBak = ctx.textAlign;
			var textBaselineBak = ctx.textBaseline;
			ctx.textAlign = args.textAlign;
			ctx.textBaseline = args.textBaseline;
			this.drawText(ctx, text, args.startPoint)
			ctx.textAlign = textAlignBak;
			ctx.textBaseline = textBaselineBak;
		} else {
			// To be implement: 其他樣式
		}
	}
}

var ChartCanvas =  {

	attributes : ["cvid", "values", "labels", "title", "theme"],

	shiftOriDist : function(c ,r, ctx) {
		if(c%2==0) return 0; //偶數邊不需要 重定原點
		var d= r*Math.cos(Math.PI/c/2);
		return (r+d)/2-d;
	},

	setUndefinedToDefault : function() {
		this._r 	 = this._r  	|| Math.min(this._width, this._height)*0.3;
	},

	setContext : function(id) {
		this._cv 	 = document.getElementById(id);
		this._width  = this._cv.width;
		this._height = this._cv.height;
		this._ctx = this._cv.getContext("2d");
		this._ctx.transform(1,0,0,-1,0,0);
		this._ctx.translate(this._width/2, -this._height/2);

	}
}
for(var key in ChartCanvas.attributes) {
	eval("ChartCanvas." + ChartCanvas.attributes[key] +" = function(data){ \
		ChartCanvas._" + ChartCanvas.attributes[key] + " = data ;\
		return this; \
	};");
}

ChartCanvas.nForce = function(id) {
	if (id==undefined) return;

	var values = this._values; log(DEBUG, values);
	if (!values) return;

	var c = values.length;
	if (c<3) return;

	if (this._cvid != id) {
		this.setContext(id);
		this._cvid = id
	};log(DEBUG, this._cvid);

	var labels = this._labels; log(DEBUG, labels);
	var title  = this._title;  log(DEBUG, title);

	this.setUndefinedToDefault();
	var ctx    = this._ctx;
	var r 	   = this._r;

	//  重定五力圖原點
	var shiftDist = this.shiftOriDist(c, r, ctx);
	if (title instanceof String) { //有標題
		// TODO: 有標題的話 武力圖 必須再往下移
		// shiftDist += ...
	} else {
		
	};
	ctx.transform(1,0,0,1,0,-shiftDist);

	Painter.drawRegularPolygon(ctx, c, r);

	var bc1 = this.bc1 || "black";
	var bc2 = this.bc2 || "white";
	var bd = this.bd || 2;
	Painter.fillLG(ctx, r, bc1, bc2, bd);

	// 頂點座標
	var vPoints = polarCoordinate.getRegularPolygonPoints(c, r);
		
	if (labels !=undefined && labels.length >= 3) {
		var labelsFormat = [];
		for(i=0;i<c;i++){
			x = vPoints[i].x;
			y = vPoints[i].y;

			var textAlign;
			var textBaseline;
			var startPoint = new Point(x,y);
			var endPoint = new Point(x,y);
			// 中線上
			if (x==0) {
				if (y>0){ // 畫在上面
						textAlign = 'center';
						textBaseline = 'bottom';
				} else {
					// 畫在下面
					textAlign = 'center';
					textBaseline = 'top';
				}
			} else if (x<0) { // 左半邊
				if (y>0) { // 畫在左上
					textAlign = 'end';
					textBaseline = 'bottom';
				} else {
					// 畫在左下
					textAlign = 'end';
					textBaseline = 'top';
				}
			} else if (y>0) { // 右半邊 // 畫在右上
				textAlign = 'start';
				textBaseline = 'bottom';
			} else { // 畫在右下
				textAlign = 'start';
				textBaseline = 'top';
			}

			labelsFormat.push({
				'textAlign':textAlign,
				'textBaseline':textBaseline,
				'startPoint':startPoint,
				'endPoint':endPoint
			})
		}

		var lc = this.lineColor || "black";
		ctx.fillStyle = lc;
		for (var i = Math.min(c, labels.length)-1; i >= 0; i--) {
			Painter.drawTextArgs(ctx, labels[i], labelsFormat[i]);
		};
	}

	// 五力分析值－數值五邊形
	var radius = [];
	for(i=0;i<c;i++)
		radius[i] = (r*values[i]/100);

	var points = polarCoordinate.getPolygonPoints(radius);

	// painter.resetLineStyle();
	ctx.strokeStyle = "#ffffff";
	Painter.drawPolygon(ctx, points);
	Painter.fillLG(ctx, r,"#0000ff","#c00000");

	// 參考線 (放射線與內五邊形)
	// painter.resetLineStyle();
	ctx.strokeStyle = "#eeeeee";
	Painter.drawRadioLines(ctx, c, r);
	Painter.drawRegularPolygon(ctx, c, r * 0.25);
	Painter.drawRegularPolygon(ctx, c, r * 0.50);
	Painter.drawRegularPolygon(ctx, c, r * 0.75);

	ctx.transform(1,0,0,1,0,shiftDist);

	return this;
}

ChartCanvas.bar = function() {
	return this;
}

ChartCanvas.pie = function() {
	if (id==undefined) return;

	this._cvid = id;
	var cvid   = this._cvid;   log(DEBUG, cvid);
	var values = this._values; log(DEBUG, values);
	var c = values.length;
	if (!values) return;
	if ( c < 1 ) return;
	var labels = this._labels; log(DEBUG, labels);
	var title  = this._title;  log(DEBUG, title);

	this.setUndefinedToDefault();
	var cv 	   = this._cv;
	var ctx    = this._ctx;
	var width  = this._width;
	var height = this._height;
	var r 	   = this._r;

	return this;
}

ChartCanvas.line = function() {
	return this;
}