

function Note(interval, dotted, pitch){
	this.interval = interval||0;
	this.dotted = dotted||0;
	this.pitch = pitch||0;
}

function ScorePaper(canvas_id) {
	this.score_id = canvas_id;
	this.cnv = document.getElementById(this.score_id);
	this.ctx = this.cnv.getContext('2d');
	this.clef = "G";
	this.temp = "拍子4/4";
	this.songtone = "C大調";
	this.radius = 10;//半徑
	this.canvasWidth = this.cnv.width;
	this.canvasHeight = this.cnv.height;
	this.all_notes = [new Note(4, 0, 0),new Note(4, 0, 1),new Note(4, 0, 2),new Note(4, 0, 3),
					  new Note(4, 0, 4),new Note(4, 0, 5),new Note(4, 0, 6),new Note(4, 0, 7),
					  new Note(2, 0, 0),new Note(2, 0, 2),new Note(2, 0, 4),new Note(2, 0, 6),
					  new Note(2, 0, 0),new Note(2, 0, 2),new Note(2, 0, 4),new Note(2, 0, 6)];
	
	//增加音符
	this.addNote = function(interval, dotted, pitch){
		this.all_notes.push(new Note(interval, dotted, pitch));
	};
	
	//繪製譜表
	this.drawStave =function(){
	  var cnv = this.cnv;
	  var canvasWidth = this.canvasWidth;
	  var canvasHeight = this.canvasHeight;
	  var ctx = this.ctx;
	  var radius = this.radius;
	  var delta = 0;//紀錄目前繪製譜表 x 錨點
	  var delta = 0;//紀錄目前繪製譜表 y 錨點
	  ctx.clearRect(0,0,canvasWidth,canvasHeight);
	  
	  //計算總小節數目
	  var all_interval = 0
	  for(var idx=0;idx<this.all_notes.length;idx++){
		all_interval += 1.0/(this.all_notes[idx]).interval;
	  }
	  
	  //計算譜表數目
	  var all_socore = all_interval/4;
	  console.log("all_interval:"+all_interval);
	  console.log("all_socore:"+all_socore);
	  	  
	  // five lines
	  var line5y = (canvasHeight/2)+((radius*2)*2);//mi
	  var line4y = (canvasHeight/2)+((radius*2)*1);//sol
	  var line3y = (canvasHeight/2)+((radius*2)*0);//si
	  var line2y = (canvasHeight/2)-((radius*2)*1);//re
	  var line1y = (canvasHeight/2)-((radius*2)*2);//fa
	  ctx.beginPath();
	  ctx.moveTo((radius*2), line5y);
	  ctx.lineTo(canvasWidth, line5y);
	  ctx.moveTo((radius*2), line4y);
	  ctx.lineTo(canvasWidth, line4y);
	  ctx.moveTo((radius*2), line3y);
	  ctx.lineTo(canvasWidth, line3y);
	  ctx.moveTo((radius*2), line2y);
	  ctx.lineTo(canvasWidth, line2y);
	  ctx.moveTo((radius*2), line1y);
	  ctx.lineTo(canvasWidth, line1y);
	  ctx.stroke();
	  console.log("line5y:"+line5y);

	  //begin bar-line
	  ctx.beginPath();
	  ctx.moveTo((radius*2), line1y);
	  ctx.lineTo((radius*2), line5y);
	  ctx.stroke();

	  //clef bar-line
	  
	  // Key signature fixme
	  drawGclef(ctx, (radius*2), line4y);
	  
	  
	  
	  //FIXME 繪製全部音符
	  delta += (radius*2)*4;
	  var interval = 0;//計算是否要繪製小節線
	  for(var idx=0;idx<this.all_notes.length;idx++){
		var note = this.all_notes[idx];
		this.drawNote(note.interval,note.dotted,note.pitch, delta, line3y);
		delta += ((radius*2)*2) * ((1.0/note.interval) / (1/4.0));
		//一個小節完成，繪製小節線
		interval += 1.0/note.interval;
		if (interval==1 && idx!=this.all_notes.length-1) {
			interval = 0;
			ctx.beginPath();
			ctx.lineWidth=1;
			ctx.moveTo(delta, line1y-1);
			ctx.lineTo(delta, line5y+1);
			ctx.stroke();
			delta += (radius*2)*2;
		}
	  }

	  //end bar-line
	  ctx.beginPath();
	  ctx.lineWidth=radius/2;
	  ctx.moveTo(canvasWidth, line1y-1);
	  ctx.lineTo(canvasWidth, line5y+1);
	  ctx.stroke();
	  ctx.beginPath();
	  ctx.lineWidth=1;
	  ctx.moveTo(canvasWidth-(radius), line1y-1);
	  ctx.lineTo(canvasWidth-(radius), line5y+1);
	  ctx.stroke();
	};
		
	//繪製單一音符
	//參數音程Interval, 0全音符,2,4,8,16,32,64,128分音符
	//附點dotted:一個附點將音長增至原來的1.5倍，兩個增至1.75倍，三個增至1.875倍
	//音高Pitch: -1為低音si, 0為do, 1為mi
	//譜表x位置
	this.drawNote=function(interval, dotted, pitch, deltax, deltay){
	  var cnv = this.cnv;
	  var ctx= this.ctx;
	  var canvasWidth = this.canvasWidth;
	  var canvasHeight = this.canvasHeight;
	  var radius = this.radius;
	  //ctx.clearRect(0,0,canvasWidth,canvasHeight);

	  var x = deltax;
	  var y = deltay+(6*radius) - (pitch*radius);
	  console.log("note x:"+x +" y:"+y +" canvasHeight:"+canvasHeight+" radius:"+radius+" pitch:"+pitch);
	  
	  //繪製符頭
	  var degrees = 360;//角度
	  var startAngle = 0;//繪製起始點
	  var endAngle = (Math.PI/180)*degrees;//繪製結束點
	  var anticlockwise = false;//順逆時針註記
	  ctx.moveTo(x, y/0.7);
	  ctx.beginPath();
	  ctx.lineWidth = "2"; 
	  ctx.save(); 
	  ctx.scale(1, 0.7);//利用變形做出橢圓
	  ctx.arc(x, y/0.7, radius, startAngle, endAngle, anticlockwise);
	  ctx.restore();
	  ctx.closePath();
	  //四分音符以下才要畫實心
	  if (interval>=4) {
		  ctx.fillStyle = 'rgb(0,0,0)';
		  ctx.fill();
	  }
	  ctx.stroke();

	  //繪製符杆2分音符
	  if (interval==2) {
		  ctx.beginPath();
		  ctx.moveTo(x+radius, y);
		  ctx.lineTo(x+radius, y-(radius*4));
		  ctx.stroke();
	  } else if (interval==4){
		  ctx.beginPath();
		  ctx.moveTo(x+radius, y);
		  ctx.lineTo(x+radius, y-(radius*4));
		  ctx.stroke();
	  }
	  
	  //繪製附點音符
	  if(dotted>0){
	  //TODO
	  }
	  
	  //繪製線條pitch<=1 及 pitch>=12
	  if ((pitch<=0 || pitch>=12)) {
		  ctx.beginPath();
		  /*TODO
		  //加線(Ledger lines)
		  if(pitch<0){
			var line = ((-1*pitch)/2)+1;
			while(line>0){
			
				var liney = (canvasHeight/2)+((radius*2)*(2+line));
				ctx.moveTo(x-17, liney);
				ctx.lineTo(x+17, liney);
				ctx.stroke();
				line=line-1;
			}
		  }*/
		  if(pitch%2==0) {
			  ctx.moveTo(x-17, y);
			  ctx.lineTo(x+17, y);
			  ctx.stroke();
		  }
	  }
	};
}


//FIXME 還要加入指定位置及放大縮小
function drawGclef(ctx, x , y) {
  ctx.save();
  ctx.beginPath();
  ctx.moveTo(0,0);
  ctx.lineTo(15.186,0);
  ctx.lineTo(15.186,40.768);
  ctx.lineTo(0,40.768);
  ctx.closePath();
  ctx.clip();
  ctx.strokeStyle = 'rgba(0,0,0,0)';
  ctx.lineCap = 'butt';
  ctx.lineJoin = 'miter';
  ctx.miterLimit = 4;
  ctx.save();
  ctx.beginPath();
  ctx.moveTo(12.049,3.5296);
  ctx.bezierCurveTo(12.354,6.6559,10.03,9.1859,7.971799999999999,11.231);
  ctx.bezierCurveTo(7.036899999999999,12.128,7.816799999999999,11.379,7.328099999999999,11.825);
  ctx.bezierCurveTo(7.225899999999999,11.346,7.029499999999999,10.094,7.047899999999999,9.715);
  ctx.bezierCurveTo(7.178299999999999,7.0211,9.3677,3.1274999999999995,11.286,1.6913999999999998);
  ctx.bezierCurveTo(11.594999999999999,2.2680999999999996,11.849,2.3145,12.049,3.5296);
  ctx.closePath();
  ctx.moveTo(12.7,19.671599999999998);
  ctx.bezierCurveTo(11.468,18.7656,9.85,18.5276,8.366399999999999,18.786599999999996);
  ctx.bezierCurveTo(8.175099999999999,17.531599999999997,7.983699999999999,16.276599999999995,7.792399999999999,15.022599999999997);
  ctx.bezierCurveTo(10.142999999999999,12.693599999999996,12.698999999999998,9.990399999999998,12.832999999999998,6.4831999999999965);
  ctx.bezierCurveTo(12.891999999999998,4.251199999999996,12.556999999999999,1.8117999999999963,11.154999999999998,-0.00040000000000350866);
  ctx.bezierCurveTo(9.454599999999997,0.1278299999999965,8.255499999999998,2.1555999999999966,7.353099999999998,3.4160999999999966);
  ctx.bezierCurveTo(5.864199999999998,6.086599999999997,6.211699999999998,9.332999999999997,6.7830999999999975,12.212599999999997);
  ctx.bezierCurveTo(5.973699999999997,13.164599999999997,4.853499999999998,13.955599999999997,4.055699999999998,14.946599999999997);
  ctx.bezierCurveTo(1.699599999999998,17.254599999999996,-0.352800000000002,20.376599999999996,0.05109999999999815,23.824599999999997);
  ctx.bezierCurveTo(0.23441999999999816,27.158599999999996,2.640499999999998,30.258599999999998,5.921299999999998,31.051599999999997);
  ctx.bezierCurveTo(7.166999999999998,31.3666,8.485199999999997,31.397599999999997,9.745399999999998,31.150599999999997);
  ctx.bezierCurveTo(9.9653,33.4006,10.771999999999998,35.779599999999995,9.837899999999998,37.9636);
  ctx.bezierCurveTo(9.137199999999998,39.5616,7.050399999999998,40.9676,5.505399999999998,40.1556);
  ctx.bezierCurveTo(4.905999999999998,39.8396,5.391699999999998,40.1046,5.027399999999998,39.9036);
  ctx.bezierCurveTo(6.097199999999998,39.6466,7.026999999999998,38.867599999999996,7.287399999999998,38.3386);
  ctx.bezierCurveTo(8.125199999999998,36.8746,6.887599999999998,34.6996,5.131999999999998,34.9806);
  ctx.bezierCurveTo(2.869999999999998,35.0266,1.941599999999998,38.1206,3.396399999999998,39.665600000000005);
  ctx.bezierCurveTo(4.743199999999998,41.18560000000001,7.229399999999998,40.9776,8.8265,39.9836);
  ctx.bezierCurveTo(10.639,38.8036,10.866,36.439600000000006,10.658999999999999,34.421600000000005);
  ctx.bezierCurveTo(10.588999999999999,33.74360000000001,10.255999999999998,31.751600000000003,10.214999999999998,31.034600000000005);
  ctx.bezierCurveTo(10.911999999999997,30.785600000000006,10.423999999999998,30.975600000000004,11.407999999999998,30.585600000000003);
  ctx.bezierCurveTo(14.067999999999998,29.532600000000002,15.764999999999997,26.326600000000003,15.001999999999997,23.463600000000003);
  ctx.bezierCurveTo(14.683999999999997,21.994600000000002,13.957999999999997,20.5496,12.699999999999998,19.671600000000005);
  ctx.closePath();
  ctx.moveTo(13.260999999999997,25.428600000000003);
  ctx.bezierCurveTo(13.474999999999998,27.419600000000003,12.207999999999998,29.7496,10.181999999999997,30.388600000000004);
  ctx.bezierCurveTo(10.045999999999998,29.593600000000002,10.009999999999996,29.377600000000005,9.919399999999996,28.913600000000002);
  ctx.bezierCurveTo(9.437199999999995,26.4536,9.175399999999996,23.9266,8.803399999999996,21.4326);
  ctx.bezierCurveTo(10.427999999999997,21.2646,12.260999999999996,21.9756,12.825999999999997,23.616600000000002);
  ctx.bezierCurveTo(13.069999999999997,24.193600000000004,13.168999999999997,24.8136,13.260999999999997,25.428600000000003);
  ctx.closePath();
  ctx.moveTo(8.112399999999997,30.6246);
  ctx.bezierCurveTo(5.568299999999997,30.7656,3.112899999999997,29.029600000000002,2.4780999999999977,26.5436);
  ctx.bezierCurveTo(1.7290999999999976,24.390600000000003,1.9497999999999978,21.913600000000002,3.2987999999999977,20.0396);
  ctx.bezierCurveTo(4.413899999999998,18.337600000000002,5.905299999999998,16.9346,7.327399999999997,15.4966);
  ctx.bezierCurveTo(7.510399999999997,16.6236,7.693399999999997,17.750600000000002,7.876399999999998,18.878600000000002);
  ctx.bezierCurveTo(4.885799999999998,19.660600000000002,2.8717999999999977,23.6036,4.661399999999998,26.329600000000003);
  ctx.bezierCurveTo(5.193799999999998,27.093600000000002,6.6378999999999975,28.5526,7.426899999999998,27.963600000000003);
  ctx.bezierCurveTo(6.324899999999998,27.280600000000003,5.423599999999999,26.104600000000005,5.617399999999998,24.736600000000003);
  ctx.bezierCurveTo(5.5352999999999986,23.454600000000003,6.987299999999998,21.8256,8.268699999999999,21.538600000000002);
  ctx.bezierCurveTo(8.707099999999999,24.407600000000002,9.209999999999999,27.611600000000003,9.648399999999999,30.4816);
  ctx.bezierCurveTo(9.142999999999999,30.5816,8.627299999999998,30.6246,8.1124,30.6246);
  ctx.closePath();
  ctx.fill();
  ctx.stroke();
  ctx.restore();
  ctx.restore();
};
