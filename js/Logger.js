window.Logger = new function() {

	// 歷程記錄等級
	this.ERROR   = 2;
	this.WARNING = 3;
	this.INFO    = 4;
	this.DEBUG   = 5;

	/**
	 * 記錄輸出方式
	 * - console 輸出到主控台
	 * - #ID 輸出到指定 HTML 元件
	 */
	this.target = "console";

	/**
	 * 記錄層級限制
	 */
	this.level  = this.DEBUG;

	/**
	 * Console 歷程記錄
	 *
	 * @param level 重要性 (1:ERROR, 2:WARNING, 3:INFO, 4:DEBUG)
	 * @param msg   記錄訊息
	 *
	 * @todo IE (console==undefined) 處理
	 */
	var log = function(level, msg) {
		if(level>Logger.level) return;
		if(console==undefined) return;

		// 產生本地時間字串
		var now = new Date();
		var hh = now.getHours();
		var mm = now.getMinutes();
		var ss = now.getSeconds();
		hh = (hh<10) ? ("0"+hh) : (""+hh);
		mm = (mm<10) ? ("0"+mm) : (""+mm);
		ss = (ss<10) ? ("0"+ss) : (""+ss);
		var time = [hh,mm,ss].join(":");

		// 產生輸出形式
		var out = "[{time}] {msg}";
		out = out.replace("{time}",time);
		out = out.replace("{msg}",msg);

		// 產生錯誤訊息
		switch(level) {
			case Logger.ERROR:   console.error(out); break;
			case Logger.WARNING: console.warn(out);  break;
			case Logger.INFO:    console.log(out);   break;
			case Logger.DEBUG:
				if(console.debug==undefined) {
					console.log(out); // for MSIE 10
				} else {
					console.debug(out);
				}
				break;
		};
	};

	/**
	 * 記錄除錯訊息
	 */
	this.debug = function(msg) {
		log(this.DEBUG,msg);
	};

	/**
	 * 記錄一般資訊
	 */
	this.info = function(msg) {
		log(this.INFO,msg);
	};

	/**
	 * 記錄警告訊息
	 */
	this.warning = function(msg) {
		log(this.WARNING,msg);
	};

	/**
	 * 記錄錯誤訊息
	 */
	this.error = function(msg) {
		log(this.ERROR,msg);
	};

};