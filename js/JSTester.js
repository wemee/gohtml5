/**
 * JSTester Javascript 測試工具
 *
 * @namespace JSTester
 * @author Raymond Wu
 */
window.JSTester = new function() {

	/**
	 * 顯示 object 內容.
	 *
	 * @param oRef   (object) object 實體
	 * @param oName  (string) object 顯示名稱
	 * @param filter (string) 過濾資料型態 (string/number/boolean)
	 */
	this.alertObj = function(oRef, oName, filter) {
		if(oRef==undefined) {
			alert(oName+"=undefined");
			return;
		}

		if(oName==undefined)  oName  = "obj";
		if(filter==undefined) filter = "all";

		var t, v, ln;
		var msg = "";
		var pattern = "({type}) {name}.{member} = {value}\n";

		for(k in oRef) {
			try {
				t = typeof(oRef[k]);
				if(filter=="all" || filter==t) {
					if(t!="function" && t!="object" && t!="undefined") {
						if(t=="string") {
							v = this.abbr(oRef[k]);
						} else {
							v = oRef[k];
						}
					} else {
						v = "...";
					}

					ln = pattern;
					ln = ln.replace("{type}",t);
					ln = ln.replace("{name}",oName);
					ln = ln.replace("{member}",k);
					ln = ln.replace("{value}",v);

					msg += ln;
				}
			} catch(e) {
				// 不可讀取的成員
				ln = pattern;
				ln = ln.replace("{type}","unknown");
				ln = ln.replace("{name}",oName);
				ln = ln.replace("{member}",k);
				ln = ln.replace("{value}","cannot access");
				msg += ln;
			}
		}

		alert(msg);
	};

	/**
	 * 顯示 object 樹狀結構.
	 */
	this.alertObjTree = function(oRef, oName, maxLevel) {
		if(oName==undefined)  oName  = "obj";
		if(maxLevel==undefined) maxLevel = 2;
		alert(this.visitObjTree(oRef,oName,0,maxLevel));
	};

	/**
	 * 顯示 object 樹狀結構.
	 *
	 * 會 GG 的寫法:
	 * - JSTester.writeObjTree(document,"document");
	 * - JSTester.writeObjTree(window,"window");
	 */
	this.writeObjTree = function(oRef, oName, maxLevel) {
		if(oName==undefined) oName = "obj";
		if(maxLevel==undefined) maxLevel = 3;
		document.write("<pre>");
		document.write(this.visitObjTree(oRef,oName,0,maxLevel));
		document.write("</pre>");
	};

	/**
	 * 顯示 object 樹狀結構.
	 */
	this.alertApiTree = function(oRef, oName, maxLevel) {
		if(oName==undefined)  oName  = "obj";
		if(maxLevel==undefined) maxLevel = 2;
		alert(this.visitApiTree(oRef,oName,0,maxLevel));
	};

	/**
	 * 樹狀拜訪物件 (不要亂動，沒寫好會 GG)
	 */
	this.visitObjTree = function(oRef, oName, curLevel, maxLevel) {
		try {
			var oType = typeof(oRef);

			// 集合資料
			// Object, Array, null 的 typeof 都是 "object"
			if(oType=="object") {
				if(curLevel==maxLevel) return oName+".* (忽略)\n";

				var msg = ""
				var isArray = (Object.prototype.toString.call(oRef)==="[object Array]");
				var pat = isArray ? "{this}[{member}]" : "{this}.{member}";

				for(k in oRef) {
					cName = pat;
					cName = cName.replace("{this}",oName);
					cName = cName.replace("{member}",k);
					if(typeof(oRef[k])!="function") {
						msg += this.visitObjTree(oRef[k], cName, curLevel+1, maxLevel);
					}
				}

				return msg;
			}

			// 單筆資料
			if(oType=="string" || oType=="number" || oType=="boolean") {
				return oName+"="+oRef+" ("+oType+")\n";
			} else {
				return oName+"=("+oType+")\n";
			}
		} catch(e) {

		}
	};

	/**
	 * 樹狀拜訪物件 (不要亂動，沒寫好會 GG)
	 */
	this.visitApiTree = function(oRef, oName, curLevel, maxLevel) {
		try {
			var oType = typeof(oRef);

			// 集合資料
			// Object, Array, null 的 typeof 都是 "object"
			if(oType=="object") {
				if(curLevel==maxLevel) return oName+".* (忽略)\n";

				var msg = ""
				var isArray = (Object.prototype.toString.call(oRef)==="[object Array]");

				if(!isArray) {
					for(k in oRef) {
						cName = "{this}.{member}";
						cName = cName.replace("{this}",oName);
						cName = cName.replace("{member}",k);
						msg += this.visitApiTree(oRef[k], cName, curLevel+1, maxLevel);
					}
				}

				return msg;
			}

			// API
			if(oType=="function") {
				return oName+"= function()\n";
			} else if(oType=="undefined") {
				return oName+"= undefined\n";
			} else {
				return "";
			}
		} catch(e) {
			// 遇到不可存取的成員
			return "";
		}
	};

	// 內部方法, 節錄字串
	this.abbr = function(str) {
		return (str.length<20) ? str : (str.substr(0,20) + " ...");
	};

};